import { Location } from "../interfaces/location";
import { Timers } from "../timers";
import { User as DBUser } from "../interfaces/db";

interface Measurement {
    location: Location;
    at: number;
}

export class LocationDangerDetector {
    static MINIMUM_VELOCITY = 0.1; // m/s
    static WATCHDOG_TIME = 5000; // miliseconds

    private userLastMovement: {[prop: string]: Measurement} = {};
    private watchdogTimers: Timers;

    public constructor(
        private danger: (uid: string) => void
    ) {
        this.watchdogTimers = new Timers(LocationDangerDetector.WATCHDOG_TIME, uid => this.timeout(uid));
    }

    private calculateDistance(source: Location, destination: Location): number {
        const earthRadiusInMeters = 6371000;
        const deg2rad = x => x * Math.PI / 180;

        let srcLat = deg2rad(source.lat);
        let srcLng = deg2rad(source.lng);

        let dstLat = deg2rad(destination.lat);
        let dstLng = deg2rad(destination.lng);

        let delta = Math.cos(srcLat) * Math.cos(dstLat) * Math.cos(dstLng - srcLng) + Math.sin(srcLat) * Math.sin(dstLat);

        return earthRadiusInMeters * Math.acos(delta);
    }

    public data(uid: string, location: Location, at: number) {
        let currentMovement: Measurement = {location, at};
        let lastMovement = this.userLastMovement[uid];

        let danger = false;

        if (lastMovement && lastMovement.at < currentMovement.at) {
            let distance = this.calculateDistance(lastMovement.location, currentMovement.location);
            let time = (currentMovement.at - lastMovement.at) / 1000;

            console.log("User", uid, "moved by", distance, "meters in", time, "seconds");

            this.watchdogTimers.stop(uid);

            danger = distance / time < LocationDangerDetector.MINIMUM_VELOCITY;
        }

        if (danger) {
            this.danger(uid);
        } else {
            // console.log("User", uid, "starting watchdog timer");
            this.watchdogTimers.start(uid);
        }

        this.userLastMovement[uid] = currentMovement;
    }

    private timeout(uid: string) {
        console.log("User", uid, "watchdog timer timed out");
        this.danger(uid);
    }

    public stop(uid: string) {
        const lastMovement = this.userLastMovement[uid];

        if (lastMovement) {
            console.log("User", uid, "has stopped tracking");

            this.watchdogTimers.stop(uid);
            delete this.userLastMovement[uid];
        }
    }
}
