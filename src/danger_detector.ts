import { Location } from "./interfaces/location";
import { LocationDangerDetector } from "./detectors/location";
import { User as DBUser } from "./interfaces/db";


export class DangerDetector {
    private locationDetector;

    public constructor(
        private danger: (uid: string)=>void
    ) {
        this.locationDetector = new LocationDangerDetector(danger);
    }

    public location(uid: string, location: Location, at: number) {
        this.locationDetector.data(uid, location, at);
    }

    public stop(uid: string) {
        this.locationDetector.stop(uid);
    }
}
