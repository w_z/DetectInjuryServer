import { User as DBUser, Location as DBLocation } from "./interfaces/db";
import { Location } from "./interfaces/location";
import * as admin from "firebase-admin";
import App = admin.app.App;
import { DangerDetector } from "./danger_detector";
import { Timers } from "./timers";

export class UserHandler {
    static PREDANGER_TIME = 10000;

    private usersCache = {};
    private detector: DangerDetector;
    private predangerTimers: Timers;

    public constructor(
        private firebase: App
    ) {
        this.detector = new DangerDetector(uid => {
            console.log("User", uid, "is potentially in danger!");

            this.firebase.database().ref(`users/${uid}`).update({
                danger: "maybe"
            });
        });

        this.predangerTimers = new Timers(UserHandler.PREDANGER_TIME, (uid) => {
            console.log("User", uid, "didn't stop timer and is now in danger!");

            this.firebase.database().ref(`users/${uid}`).update({
                danger: "yes"
            });
        });
    }

    public attachListeners() {
        const database = this.firebase.database();

        const usersRef = database.ref("users/");
        usersRef.on("child_added", event => this.added(event.key, event.val()));
        usersRef.on("child_changed", event => this.changed(event.key, event.val()));
        usersRef.on("child_removed", event => this.removed(event.key, event.val()));

        usersRef.on("child_moved", (data) => console.debug("Database: moved: ", data.key, data.val()));

        database.ref("locations/").on("child_added", event => {

            let data: DBLocation = event.val();

            let loc: Location = {
                lat: data.lat,
                lng: data.lng
            };

            this.newLocation(data.uid, loc, data.at);
        });

    }

    private added(uid: string, data: DBUser) {
        console.log(`New user #${uid} with nick "${data.name}"`);

        this.check(uid, data);
    }

    private check(uid: string, data: DBUser) {
        const cached = this.usersCache[uid] || data;

        if (!cached.tracking && data.tracking) {
            console.log("User", uid, "is now tracking");
        } else if (!data.tracking) {
            this.detector.stop(uid);
            this.predangerTimers.stop(uid);

            // console.log(`Archive track history for ${uid}`);
            // status changed
            this.archive(uid);

            if (data.danger !== "no") {
                console.log("User", uid, "was '", data.danger, "' in danger but is not tracking, setting not in danger");
                this.firebase.database().ref(`users/${uid}`).update({
                    danger: "no"
                });
            }
        }

        this.usersCache[uid] = data;

        if (cached.danger !== data.danger) {
            if (data.danger === 'maybe') {
                console.log("User", uid, "is maybe in danger");

                this.predangerTimers.start(uid);
            } else {
                if (data.danger === "yes") {
                    console.log("User", uid, "is now in danger!");
                } else if (data.danger === "no") {
                    console.log("User", uid, "said is not in danger");
                }

                this.predangerTimers.stop(uid);
            }
        }
    }

    private changed(uid: string, data: DBUser) {
        console.log(`Changed user #${uid} with nick "${data.name}"`);

        this.check(uid, data);
    }

    private removed(uid: string, data: DBUser) {
        console.log(`Removed user #${uid} with nick "${data.name}"`);

        delete this.usersCache[uid];
    }

    private newLocation(uid: string, loc: Location, at: number) {
        this.predangerTimers.stop(uid);

        if (this.usersCache[uid] && this.usersCache[uid].tracking && this.usersCache[uid].danger !== "yes") {
            this.detector.location(uid, loc, at);
        }

        // console.log("User", uid, "is in", loc.lat, loc.lng, "at time", at);
    }

    private getUser(uid: string): Promise<DBUser> {
        if (this.usersCache[uid]) {
            return Promise.resolve(this.usersCache[uid]);
        }

        return new Promise(resolve => {
            this.firebase.database().ref(`users/${uid}`).once("value", e => {
                this.usersCache[uid] = e.val();

                resolve(e.val());
            });
        });
    }

    private archive(uid: string) {
      const ref = this.firebase.database().ref('locations');
      ref.once("value", e => {
        const responseObject = e.val();

        // const filteredArray = lodash.filter(responseObject, { uid: uid });
        const filteredArray = [];

        let count = 0;

        // filter data
        for (const locationKey in responseObject) {
          let location: any = responseObject[locationKey];
          if (location.uid === uid) {
            // for future
            filteredArray.push(location);

            count++;

            // delete from locations
            ref.child(locationKey).remove();
          }
        }

        console.log("Archived", count, "locations for", uid);

      });
    }

}
